<?php

/**
 * @file
 * Main module functions for the extra_field_description module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldFilteredMarkup;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Implements hook_field_widget_third_party_settings_form().
 */
function extra_field_description_field_widget_third_party_settings_form(WidgetInterface $plugin, FieldDefinitionInterface $field_definition, $form_mode, $form, FormStateInterface $form_state) {
  if ($field_definition->getFieldStorageDefinition()->isBaseField() == FALSE) {
    $plugin_third_party_settings = $plugin->getThirdPartySettings('extra_field_description');
    // Check if we have access to extra fields.
    if (\Drupal::currentUser()->hasPermission('administer field prefix')) {
      // Append extra fields in field settings form.
      $form['extra_description'] = [
        '#type' => 'details',
        '#title' => t("Extra description settings"),
        '#weight' => -4,
        '#open' => TRUE,
      ];

      $form['extra_description']['multiple'] = [
        '#type' => 'checkbox',
        '#title' => t('For each line'),
        '#description' => t("If field is multiple and specified checkbox selected - extra description will be written above each field."),
        '#default_value' => isset($plugin_third_party_settings['extra_description']['multiple']) ? $plugin_third_party_settings['extra_description']['multiple'] : FALSE,
      ];
      $form['extra_description']['over_description'] = [
        '#type' => 'textarea',
        '#title' => t('Extra description'),
        '#description' => t("Extra description over the field.<br />Allowed HTML tags: @tags", ['@tags' => FieldFilteredMarkup::displayAllowedTags()]),
        '#default_value' => isset($plugin_third_party_settings['extra_description']['over_description']) ? $plugin_third_party_settings['extra_description']['over_description'] : '',
      ];

      $plugin->setThirdPartySetting('extra_field_description', 'extra_description', $form['extra_description']);

      return $form;
    }
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function extra_field_description_field_widget_form_alter(&$element, FormStateInterface $form_state, $context) {
  $third_party_settings = $context['widget']->getThirdPartySettings('extra_field_description', 'extra_description');
  if (!empty($third_party_settings)) {
    if (isset($element['value'])) {
      if ($element['value']['#type'] == 'datetime' || $element['value']['#type'] == 'datelist') {
        $element['#field_prefix'] = '<div class="extra-description" >' . $third_party_settings['extra_description']['over_description'] . '</div>';
      }
      else {
        $element['value']['#field_prefix'] = '<div class="extra-description" >' . $third_party_settings['extra_description']['over_description'] . '</div>';
      }
    }
    elseif (isset($element['target_id'])) {
      $element['target_id']['#field_prefix'] = '<div class="extra-description" >' . $third_party_settings['extra_description']['over_description'] . '</div>';
    }
    else {
      $element['#field_prefix'] = '<div class="extra-description" >' . $third_party_settings['extra_description']['over_description'] . '</div>';
    }
  }
}

/**
 * Implements hook_page_attachments().
 */
function extra_field_description_page_attachments(array &$attachments) {
  $attachments['#attached']['library'][] = 'extra_field_description/extra_field_description_css';
}
