<?php

namespace Drupal\custom_rest\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "quiz_rest_resource",
 *   label = @Translation("Custom quiz rest resource"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/quiz",
 *     "https://www.drupal.org/link-relations/create" = "/api/quiz"
 *   }
 * )
 */
class QuizRestResource extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('ccms_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param $node_type
   * @param $data
   * @return \Drupal\rest\ResourceResponse Throws exception expected.
   * Throws exception expected.
   */
  public function post($data) {
    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    
    if(!empty($data['field_srh_quiz_question']['value'])){
        foreach ($data['field_srh_quiz_question']['value'] as $key => $value) {
        $field_srh_quiz_question[] = ['target_id' => $value];
            }
        }
     $response_node_array =  array(
        'type' => 'srh_quiz_response',
        'title' => $data['title']['value'],
        'field_srh_quiz' => ['target_id' => $data['field_srh_quiz']['value']],
        'field_respondent_age_group'=>  $data['field_respondent_age_group']['value'] ,
        'field_respondent_country' =>   $data['field_respondent_country']['value'],
        'field_respondent_unique_id'  =>  $data['field_respondent_unique_id']['value'],
        'field_respondent_gender' => $data['field_respondent_gender']['value'],
        'field_respondent_district' => $data['field_respondent_district']['value'],
        'field_quiz_status' => $data['field_quiz_status']['value'],
        'field_srh_quiz_question' => $field_srh_quiz_question,
        'status' => 0  
      ); 

    $node = Node::create($response_node_array);
    $node->save();  
    return new ResourceResponse($node);
  }

}