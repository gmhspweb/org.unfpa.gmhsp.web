<?php

namespace Drupal\custom_rest\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "survey_poll_response",
 *   label = @Translation("Custom Survey Poll rest response"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/survey_poll_response",
 *     "https://www.drupal.org/link-relations/create" = "/api/survey_poll_response"
 *   }
 * )
 */
class SurveyPollResponse extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('ccms_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param $node_type
   * @param $data
   * @return \Drupal\rest\ResourceResponse Throws exception expected.
   * Throws exception expected.
   */
  public function post($data) {
      
   // echo "<pre>";
    //print_r($data['field_srh_quiz_question']['value']);
    //echo "</pre>";
    //exit();

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }

    
      
     $node = Node::create(
      array(
        'type' => 'srh_survey_poll_response',
        'title' => $data['title']['value'],
        'field_user_responses'=> $data['field_user_responses']['value'],
        'field_user_age_group' => $data['field_user_age_group']['value'],
        'field_country' => ['target_id' => $data['field_country']['value']],
        'field_education_level' => $data['field_education_level']['value'],
        'field_extra_profile_data' => $data['field_extra_profile_data']['value'],
        'field_tips_gender' => $data['field_tips_gender']['value'],
        'field_unique_identifier' => $data['field_unique_identifier']['value'],
        'field_survey_poll_reference' => ['target_id' => $data['field_survey_poll_reference']['value']],
        'status' => 0  
      )
    );

      
    $node->save();

    return new ResourceResponse($data['field_survey_poll_reference']['value']);
  }

}