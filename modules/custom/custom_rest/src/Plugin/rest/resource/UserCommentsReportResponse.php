<?php

namespace Drupal\custom_rest\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;
use Drupal\comment\Entity\Comment;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "user_comments_report_response",
 *   label = @Translation("Custom User Comments Report rest response"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/user_comments_report_response",
 *     "https://www.drupal.org/link-relations/create" = "/api/user_comments_report_response"
 *   }
 * )
 */
class UserCommentsReportResponse extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('ccms_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param $node_type
   * @param $data
   * @return \Drupal\rest\ResourceResponse Throws exception expected.
   * Throws exception expected.
   */
  public function post($data) {
      
   // echo "<pre>";
   //  print_r($data);
   //  echo "</pre>";
   //  exit();

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }
    $comment_id = $data['comment_id']['value'];
    $comment_entity = Comment::load($comment_id);
    $comment_entity->field_comment_report = $data['comment_report']['value'];
    $reported_user = $comment_entity->get('field_reported_users')->getValue();
    if(empty($reported_user)){
      $comment_entity->field_reported_users = $data['nick_name']['value'];
    }
    else{
      $existing_user = $reported_user['0']['value'];
      $existing_users = explode(',',$existing_user);
      $existing_users[] = $data['nick_name']['value'];
      $existing_users = array_unique($existing_users);
      $reported_users = implode(',', $existing_users);
      $comment_entity->field_reported_users = $reported_users;
    }
    $comment_entity->save();
  
  return new ResourceResponse($data['nick_name']['value']);
  }

}