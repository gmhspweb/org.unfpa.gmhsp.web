<?php

namespace Drupal\custom_rest\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;
use Drupal\comment\Entity\Comment;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "user_comments_response",
 *   label = @Translation("Custom User Comments rest response"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/user_comments_response",
 *     "https://www.drupal.org/link-relations/create" = "/api/user_comments_response"
 *   }
 * )
 */
class UserCommentsResponse extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('ccms_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param $node_type
   * @param $data
   * @return \Drupal\rest\ResourceResponse Throws exception expected.
   * Throws exception expected.
   */
  public function post($data) {
      
   // echo "<pre>";
   //  print_r($data);
   //  echo "</pre>";
   //  exit();

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }
    $values = [
    'entity_type' => 'node',
    'entity_id'   =>  $data['entity_id']['value'],
    'field_name'  => 'field_user_comments',
    'uid' => 0,
    'comment_type' => 'user_comment',
    'subject' => $data['nick_name']['value'] ,
    'comment_body' => $data['user_comment']['value'],
    'status' => 0,
    'pid' => $data['parent_comment_id']['value'],
    'field_unique_identifier' => $data['field_unique_identifier']['value'],
    'field_consent_to_publish' => $data['field_consent_to_publish']['value'],
    'field_consent_to_share_on_social' => $data['field_consent_to_share_on_social']['value'],
    'field_age_group' => $data['field_age_group']['value'],
    'field_country' =>['target_id' => $data['field_country']['value']],
    'field_education_level' => $data['field_education_level']['value'],
    'field_extra_profile_data' => $data['field_extra_profile_data']['value'],
    'field_gender' =>$data['field_gender']['value'],
    'field_srh_content_item_reference' => ['target_id' => $data['entity_id']['value']],
  ];
  $comment = Comment::create($values);
  $comment->save();
  
  return new ResourceResponse($data['nick_name']['value']);
  }

}