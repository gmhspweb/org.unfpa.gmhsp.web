<?php

namespace Drupal\custom_rest\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "user_poll_count_response",
 *   label = @Translation("Custom User Poll Count rest response"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/user_poll_count_response",
 *     "https://www.drupal.org/link-relations/create" = "/api/user_poll_count_response"
 *   }
 * )
 */
class UserPollCountResponse extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('ccms_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param $node_type
   * @param $data
   * @return \Drupal\rest\ResourceResponse Throws exception expected.
   * Throws exception expected.
   */
  public function post($data) {
      
   // echo "<pre>";
   //  print_r($data);
   //  echo "</pre>";
   //  exit();

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }
    $poll_id = $data['poll_id']['value'];
    $poll_entity = Node::load($poll_id);
    $question = $poll_entity->field_srh_survey_question->getValue();
    // Loop through the result set.
    foreach ( $question as $element ) {
      if($element['target_id'] == $data['question_id']['value']){
        $question_data = \Drupal\paragraphs\Entity\Paragraph::load( $element['target_id'] );
        $response_text = $question_data->field_response_options->getValue();
        foreach ($response_text as $response_text_value) {
            $response_option[] = $response_text_value['value'];
        }
      if(!empty($data['user_response_option']['value']) && in_array($data['user_response_option']['value'], $response_option)){
      foreach ($response_option as $key => $response) {
        if($response == $data['user_response_option']['value']){
          $field_poll_count = $question_data->get('field_poll_result_count')->getValue();
          $field_poll_count[$key]['value'] = $field_poll_count[$key]['value'] + 1;
          $response_data['poll_option_value'] = $data['user_response_option']['value'];
          $response_data['poll_result_count'] = $field_poll_count[$key]['value'];
          $question_data->field_poll_result_count = $field_poll_count;
          $question_data->save();
        }
        $poll_entity->save();
        }
      }
      }
    }
  return new ResourceResponse($response_data);
  }

}