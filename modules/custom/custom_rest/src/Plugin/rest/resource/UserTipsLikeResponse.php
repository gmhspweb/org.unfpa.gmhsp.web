<?php

namespace Drupal\custom_rest\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\node\Entity\Node;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "user_tips_like_response",
 *   label = @Translation("Custom User Tips Like rest response"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/user_tips_like_response",
 *     "https://www.drupal.org/link-relations/create" = "/api/user_tips_like_response"
 *   }
 * )
 */
class UserTipsLikeResponse extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('ccms_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param $node_type
   * @param $data
   * @return \Drupal\rest\ResourceResponse Throws exception expected.
   * Throws exception expected.
   */
  public function post($data) {
      
   // echo "<pre>";
   //  print_r($data);
   //  echo "</pre>";
   //  exit();

    // You must to implement the logic of your REST Resource here.
    // Use current user after pass authentication to validate access.
    if (!$this->currentUser->hasPermission('access content')) {
      throw new AccessDeniedHttpException();
    }
    $tips_id = $data['tips_id']['value'];
    $tips_entity = Node::load($tips_id);

    $field_tips_like_count = $tips_entity->get('field_tips_like_count')->getValue();
    if(empty($field_tips_like_count)){
      $tips_entity->field_tips_like_count = $data['liked_tips']['value'];
    }
    else{
      $existing_liked_tip_count = $field_tips_like_count['0']['value'];
      $new_liked_tips_count = $existing_liked_tip_count + 1;
      $tips_entity->field_tips_like_count = $new_liked_tips_count;
    }
    $tips_liked_user = $tips_entity->get('field_users_who_liked_the_tips')->getValue();
    if(empty($tips_liked_user)){
      $tips_entity->field_users_who_liked_the_tips = $data['nick_name']['value'];
    }
    else{
      $existing_user = $tips_liked_user['0']['value'];
      $existing_users = explode(',',$existing_user);
      $existing_users[] = $data['nick_name']['value'];
      $existing_users = array_unique($existing_users);
      $tips_liked_users = implode(',', $existing_users);
      $tips_entity->field_users_who_liked_the_tips = $tips_liked_users;
    }
    $tips_entity->save();
  
  return new ResourceResponse($data['nick_name']['value']);
  }

}