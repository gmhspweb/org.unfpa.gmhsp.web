<?php

namespace Drupal\dashboard\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "srh_content_block",
 *   admin_label = @Translation("SRH Content"),
 * )
 */
class SRHContentBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
	  
	$total = db_query("SELECT count(nid) FROM {node_field_data} WHERE type='srh_content'")->fetchField();
	$count = db_query("SELECT count(nid) FROM {node_field_data} WHERE type='srh_content' AND status=1 ")->fetchField();
	$per = (100/$total)*$count;	
	$percentage = round($per,2);
	$data = $total." Total Items <br>".$percentage." % Published";

    return [
      '#markup' => $data,	  
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {  
	$current_user = \Drupal::currentUser();
	$roles = $current_user->getRoles();			
	if (in_array('administrator', $roles) || in_array('content_admin', $roles)){
		//return AccessResult::allowedIfHasPermission($account, 'access content');
		return AccessResult::allowed();
	}
	return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['my_block_settings'] = $form_state->getValue('my_block_settings');
  }
}