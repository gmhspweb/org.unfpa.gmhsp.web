<?php

namespace Drupal\dashboard\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "service_provider",
 *   admin_label = @Translation("Service Providers"),
 * )
 */
class ServiceProvider extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
	  
	$count = db_query("SELECT count(nid) FROM {node_field_data} WHERE type='srh_service_provider' AND status=1")->fetchField();  
	$counter = db_query("SELECT count(nid) FROM {node_field_data} WHERE type='srh_provider_feedback' AND status=1 ")->fetchField();
	$data = $count." Providers <br>".$counter." Feedback Items";

    return [
      '#markup' => $data,	  
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {	
	$current_user = \Drupal::currentUser();
	$roles = $current_user->getRoles();	
	if (in_array('administrator', $roles) || in_array('content_admin', $roles)){
		//return AccessResult::allowedIfHasPermission($account, 'access content');
		return AccessResult::allowed();
	}
	return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['my_block_settings'] = $form_state->getValue('my_block_settings');
  }
}