<?php
namespace Drupal\firebase_chatbot\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
/**
 * Class CustomAccessCheck.
 *
 * @package Drupal\firebase_chatbot\Access
 */
class CustomAccessCheck implements AccessInterface 
{
	
  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for the logged in user.
   */
  public function access(AccountInterface $account) {
    // User has a profile field defining their favorite color.
    
      // If the user's favorite color is blue, give them access.
    //$a = $account->roles;
     $roles = $account->getRoles();
      return AccessResult::allowed();
   
    //return AccessResult::forbidden();
  
}
}