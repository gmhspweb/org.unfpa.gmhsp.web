<?php

namespace Drupal\firebase_chatbot\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * @Block(
 *   id = "firebase_chatbot",
 *   admin_label = @Translation("Drupal Firebase Chatbot"),
 *   category = @Translation("Custom"),
 * )
 */
class AngularAppBlock extends BlockBase
{

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    /*return [
    '#theme' => 'firebase_chatbot_page',
    '#var1' => 'Variable one passed here',
  ];*/
  
    return [
      '#type' => 'html_tag',
      '#tag' => 'app-root', // Selector of the your app root component from the Angular app
      '#attached' => [
        'library' => [
          'firebase_chatbot/drupal_angular_lib', // To load the library only with this block
        ],
      ],
    ];
  }

}