<?php

namespace Drupal\firebase_chatbot\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;
/**
 * Provides a current logged in user Resource
 *
 * @RestResource(
 *   id = "current_logged_in_user",
 *   label = @Translation("Logged in user Resource"),
 *   uri_paths = {
 *     "canonical" = "/app/logged_in_user"
 *   }
 * )
 */
class CurrentUser extends ResourceBase 
{
	  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
	
	  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get() {
  	$current_user = \Drupal::currentUser();
  	$user = \Drupal\user\Entity\User::load($current_user->id());
    drupal_set_message($current_user);
  	foreach ($user->getRoles() as $key => $value) {
  		if($value == 'knowledgeable_person' or $value == 'administrator'){
    
$termreference = $user->get('field_country')->target_id;
$term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($termreference);
if($term){
$countryName = $term->getName();
}
  			$response = ['userID' => $user->id(),
        'user_family_name' => $user->field_family_name->value,
        'user_given_name' => $user->field_given_name->value,
        'user_country_name' => $countryName];
  		}
  	}
    
    return new ResourceResponse($response);
  }
}