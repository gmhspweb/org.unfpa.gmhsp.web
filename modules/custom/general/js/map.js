(function ($, Drupal, drupalSettings) {
	 Drupal.behaviors.yourbehavior = {
        attach: function (context, settings) {
			//console.log('test');
						
			$('#edit-field-address-field-1-0-value, #edit-field-municipal-region-0-value, #edit-field-city-0-value,#edit-field-postal-code-0-value, #edit-field-country ').change(function(){
				var address1 =	$('#edit-field-address-field-1-0-value').val();
				var municipal =	$('#edit-field-municipal-region-0-value').val();
				var city = $('#edit-field-city-0-value').val();
				var postal_code = $('#edit-field-postal-code-0-value').val();
				var country = $('#edit-field-country').val();
			
				var geocoder = new google.maps.Geocoder();
				//var address = "1225 4th St. NE,District of Columbia, Washington 20002, USA";
				var address = address1 + ',' + municipal + ',' + city + ',' +postal_code + ',' + country;
				geocoder.geocode( { 'address': address}, function(results, status) {
				  if (status == google.maps.GeocoderStatus.OK) {
					var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();	
					
					$("#edit-field-lat-lng-0-value").attr("readonly", false);
					$("#edit-field-latitude-0-value").attr("readonly", false);
					$("#edit-field-longitude-0-value").attr("readonly", false);					
					$('#edit-field-latitude-0-value').val(latitude);		
					$('#edit-field-longitude-0-value').val(longitude);
					$('#edit-field-lat-lng-0-value').val(latitude+' '+longitude);
					$("#edit-field-latitude-0-value").attr("readonly", true);
					$("#edit-field-longitude-0-value").attr("readonly", true);
					$("#edit-field-lat-lng-0-value").attr("readonly", true);
				  } 
				}); 
			});
			
			$("#edit-field-latitude-0-value").attr("readonly", true);
			$("#edit-field-longitude-0-value").attr("readonly", true);
			$("#edit-field-lat-lng-0-value").attr("readonly", true);
			
		}
	}
})(jQuery, Drupal, drupalSettings);