(function ($, Drupal, drupalSettings) {
	Drupal.behaviors.questiontype = {
        attach: function (context, settings) {
	$('#edit-field-question-type').change(function() {
          var selectedvalue = $("#edit-field-question-type option:selected").val();
          if(selectedvalue == 'scq'){
          	$(".paragraphs-dropbutton-wrapper").hide();
          }
          else{
          	$(".paragraphs-dropbutton-wrapper").show();
          }
      });
	}
}
})(jQuery, Drupal, drupalSettings);