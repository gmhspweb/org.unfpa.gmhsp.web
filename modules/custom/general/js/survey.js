(function ($, Drupal, drupalSettings) {
	Drupal.behaviors.questiontype = {
        attach: function (context, settings) {
  $(".field--name-field-poll-result-count .form-number").prop("readonly", true);
  $(".field--name-field-poll-result-count .field-add-more-submit").prop("disabled", true);
  $('.field--name-field-survey-poll-question-type select').change(function() {
  $(".paragraphs-subform .field-group-fieldset").show();
  //$(this).parent().parent().parent().parent().parent().parent().parent().find("fieldset").show();
          var optionSelected = $("option:selected", this);
          var valueSelected = this.value;
          if(valueSelected == '427'){
            //$(this).parent().parent().parent().parent().parent().parent().parent().find("fieldset").hide();
            $(".paragraphs-subform .field-group-fieldset").hide();
          }
      });
	}
}
})(jQuery, Drupal, drupalSettings);