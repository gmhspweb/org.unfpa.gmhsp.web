<?php

namespace Drupal\json_migrate\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * An example controller.
 */
class DeleteCommentController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content() {
    $result = \Drupal::entityQuery("comment")
    ->accessCheck(FALSE)
    ->execute();
    $storage_handler = \Drupal::entityTypeManager()->getStorage("comment");
    $entities = $storage_handler->loadMultiple($result);
    $storage_handler->delete($entities);
    drupal_set_message(t('Comments have been deleted successfully'));
        $build = [
          '#markup' => $this->t('comments deleted successfully'),
        ];
        return $build;
      }

}