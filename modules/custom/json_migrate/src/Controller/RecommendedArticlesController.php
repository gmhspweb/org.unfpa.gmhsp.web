<?php

namespace Drupal\json_migrate\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * An example controller.
 */
class RecommendedArticlesController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content($country_id) {
    $public_path = \Drupal::service('file_system')->realpath(file_default_scheme() . "://");
    if($country_id == '332'){
      $filepath = $public_path.'/tuneme_json/mw_articlepage_export.json';
    }
    elseif($country_id == '310'){
      $filepath = $public_path.'/tuneme_json/na_articlepage_export.json';
    }
    elseif($country_id == '309'){
      $filepath = $public_path.'/tuneme_json/bw_articlepage_export.json';
    }
    elseif($country_id == '333'){
      $filepath = $public_path.'/tuneme_json/sw_articlepage_export.json';
    }
    elseif($country_id == '306'){
      $filepath = $public_path.'/tuneme_json/zw_articlepage_export.json';
    }
    elseif($country_id == '307'){
      $filepath = $public_path.'/tuneme_json/zm_articlepage_export.json';
    }
    elseif($country_id == '308'){
      $filepath = $public_path.'/tuneme_json/ls_articlepage_export.json';
    }
    $data = file_get_contents($filepath);
    $json_data = json_decode($data,true);
    foreach ($json_data as $key => $content) {
      $recommeded_article_id = [];
      if(!empty($content['recommended_articles'])){
        foreach ($content['recommended_articles'] as $key => $articles) {
          $recommeded_article_id[] = $articles['recommended_article']['id'];
        }
        //Get Recommended articles node ID's 
        $langcode =  \Drupal::languageManager()->getCurrentLanguage()->getId();
        $query = \Drupal::entityQuery('node');
        $query->condition('type', 'srh_content');
        $query->condition('field_country', $country_id);
        $query->condition('field_content_id', $recommeded_article_id, 'IN');
        $query->condition('langcode', $langcode);
        $rec_article_nids = $query->execute();
        if(!empty($rec_article_nids)){
          //Get main article node and update recommended articles enity refernce field
          $query1 = \Drupal::entityQuery('node');
          $query1->condition('type', 'srh_content');
          $query1->condition('field_country', $country_id);
          $query1->condition('field_content_id', $content['id']);
          $query1->condition('langcode', $langcode);
          $base_node_nid = $query1->execute();
          foreach ($base_node_nid as $key => $nid) {
            $main_node = Node::load($nid);
          //Loop through Recommended articles and update main content
          foreach ($rec_article_nids as $rec_article_nid) {
            $main_node->field_recommended_articles[] = ['target_id' => $rec_article_nid ];
              }
          $main_node->save();
          }
          drupal_set_message(t('Content for @country has been updated successfully', ['@country' => $country_id]));
        }
      }
    }
    $build = [
      '#markup' => $this->t('Recommended Articles Updated successfully'),
    ];
    return $build;
  }

}