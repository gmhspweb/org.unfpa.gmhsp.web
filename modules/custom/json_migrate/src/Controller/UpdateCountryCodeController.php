<?php

namespace Drupal\json_migrate\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * An example controller.
 */
class UpdateCountryCodeController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content($country_id) {
        $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $query = \Drupal::entityQuery('node');
        $query->condition('type', 'srh_content');
        $query->condition('field_country', $country_id);
        $query->condition('langcode', $langcode);
        $content_ids = $query->execute();
        if(!empty($content_ids)){
          foreach ($content_ids as $key => $content_id) {
            $updated_node = Node::load($content_id);
            $updated_node->field_country = ['target_id' => '306' ]; //Zimbawe Country code
            $updated_node->save();
          }
          drupal_set_message(t('Zambaia @country country has been updated to Zimbawe successfully', ['@country' => $country_id]));
        }
    $build = [
      '#markup' => $this->t('Content Updated successfully'),
    ];
    return $build;
  }

}