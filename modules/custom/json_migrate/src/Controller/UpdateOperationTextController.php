<?php

namespace Drupal\json_migrate\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * An example controller.
 */
class UpdateOperationTextController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
 public function content($country_id) {
        $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $query = \Drupal::entityQuery('node');
        $query->condition('type', 'srh_service_provider');
        $query->condition('field_country', $country_id);
        $query->condition('langcode', $langcode);
        $query->condition('nid', 10860, '>');
        $query->sort('nid', ASC);
        $query->range(0, 500);
        $content_ids = $query->execute();
        if(!empty($content_ids)){
          foreach ($content_ids as $key => $content_id) {
            $updated_node = Node::load($content_id);
            $field_hours_of_operation = $updated_node->get('field_hours_of_operation')->getValue();
            $string = $field_hours_of_operation[0]['value'];
            $string = str_replace("p.m.", "p.m.\n", $string);
            //$string = str_replace("11:00 a.m.", "11:00 a.m.\n", $string);
            //$string = str_replace("Sun Closed\n", "Sun Closed", $string);
            //$string = str_replace("Closed", "Closed\n", $string);
            //$string = str_replace("�", "–", $string);
            $updated_node->field_hours_of_operation = $string;
            $updated_node->save();
            
          }
          drupal_set_message(t('Hours of operation text has been updated successfully for @country', ['@country' => $country_id]));
        }
    $build = [
      '#markup' => $this->t('Content Updated successfully'),
    ];
    return $build;
  }

}