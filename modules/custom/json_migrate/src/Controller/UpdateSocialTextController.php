<?php

namespace Drupal\json_migrate\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * An example controller.
 */
class UpdateSocialTextController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
 public function content($country_id) {
        // $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
        // $query = \Drupal::entityQuery('node');
        // $query->condition('type', 'srh_service_provider');
        // $query->condition('field_country', $country_id);
        // $query->condition('langcode', $langcode);
        // $content_ids = $query->execute();
        // if(!empty($content_ids)){
        //   foreach ($content_ids as $key => $content_id) {
        //     $updated_node = Node::load($content_id);
        //     $field_hours_of_operation = $updated_node->get('field_hours_of_operation')->getValue();
        //     if (preg_match('p.m.', $field_hours_of_operation[0]['value'])) {
        //          str_replace($day, "<br/>". $day, $field_hours_of_operation[0]['value']); 
        //       }
        //     //$hours_of_operation = nl2br($field_hours_of_operation);
        //     //$days = ['Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat', 'Sun', 'Sun'];
        //     //kint($field_hours_of_operation[0]['value']);exit;
        //       // foreach($days as $day) {
        //       //   $hours_of_operation = str_replace($day, "<br/>". $day, $field_hours_of_operation[0]['value']);
        //       // }
        //     $updated_node->field_hours_of_operation = $hours_of_operation;
        //     $updated_node->save();
        //   }
          drupal_set_message(t('@country country has been updated to Zimbawe successfully', ['@country' => $country_id]));
        //}
    $build = [
      '#markup' => $this->t('Content Updated successfully'),
    ];
    return $build;
  }

}