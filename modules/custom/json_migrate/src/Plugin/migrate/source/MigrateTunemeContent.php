<?php 
namespace Drupal\json_migrate\Plugin\migrate\source; 
use Drupal\migrate\Plugin\migrate\source\SourcePluginBase; 
use Drupal\migrate\Row; 
 
/** 
* Source plugin to import data from JSON files 
* @MigrateSource( 
*   id = "migrate_tuneme_content" 
* ) 
*/ 
class MigrateTunemeContent extends SourcePluginBase {

/** 
   * Initializes the iterator with the source data. 
   * @return \Iterator 
   *   An iterator containing the data for this source. 
   */ 
  protected function initializeIterator() { 
    $uri = $this->configuration['path'];
    $path = drupal_realpath($uri);
    $rows = json_decode(file_get_contents($path),true);
    return new \ArrayIterator($rows); 
  }  
 
  public function prepareRow(Row $row) { 
    $body = $row->getSourceProperty('body');
    $body_data = '';
    foreach ($body as $key => $content) {
      if($content['type'] == 'paragraph'){
          $newline_text = preg_replace("/\r\n/", "<br>", $content['value']);
          $boldtext = preg_replace('#\*{2}(.*?)\*{2}#', '<b>$1</b>', $newline_text);
          $body_data.= '<p>'.$boldtext.'</p>';
      }
      elseif($content['type'] == 'list'){
        $body_data .= '<ul>';
        foreach ($content['value'] as $key => $list) {
          $format_text = preg_replace("/\r\n/", "<br>", $list);
          $format_text_bold = preg_replace('#\*{2}(.*?)\*{2}#', '<b>$1</b>', $format_text);
          $body_data .= '<li>'.$format_text_bold.'</li>';
        }
        $body_data .= '</ul>';
      }
      elseif($content['type'] == 'numbered_list'){
        $body_data .= '<ol>';
        foreach ($content['value'] as $key => $list) {
          $format_text = preg_replace("/\r\n/", "<br>", $list);
          $format_text_bold = preg_replace('#\*{2}(.*?)\*{2}#', '<b>$1</b>', $format_text);
          $body_data .= '<li>'.$format_text_bold.'</li>';
        }
        $body_data .= '</ol>';
      }
      elseif($content['type'] == 'heading'){
        $newline_text = preg_replace("/\r\n/", "<br>", $content['value']);
        $boldtext = preg_replace('#\*{2}(.*?)\*{2}#', '<b>$1</b>', $newline_text);
        $body_data.= '<p><h4>'.$boldtext.'</h4></p>';
      }
      elseif($content['type'] == 'image'){
        $image_id = $content['value'];
        $image_url = 'http://ls.tuneme.org/api/v2/images/'.$image_id.'/';
        $image_data = file_get_contents($image_url);
        $image_json = json_decode($image_data,'true');
        $image_link = "<img src='".$image_json['file']."'>";
        $body_data.= '<p>'.$image_link.'</p>';
      }
    }
    $row->setSourceProperty('body', $body_data);
    //country
    $country = $row->getSourceProperty('featured_in_section');
    $country_code = $this->configuration['country_id'];
    $row->setSourceProperty('featured_in_section', $country_code);
    //Context Taxonomy
    $context_taxonomy = $row->getSourceProperty('meta');
    if(!empty($context_taxonomy['parent']['title'])){
      $term_name = $context_taxonomy['parent']['title'];
      $term_id = $this->get_term_id('context_taxonomy',$term_name);
      if(!empty($term_id)){
        $parent = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadParents($term_id);
        $parent = reset($parent);
        if(isset($parent) && !empty($parent)){
          $parent_id = $parent->id();
          $row->setSourceProperty('meta', $parent_id);
          $row->setSourceProperty('related_sections', $term_id);
        }
      else{
        $row->setSourceProperty('meta', $term_id);
        }
      }
      //check content as hot topic
      if($term_name == 'Hot Topics'){
          $row->setSourceProperty('feature_as_topic_of_the_day', '1');
        }
      }
    //Cover Image
    $cover_image = $row->getSourceProperty('image');
    if(!empty($cover_image['meta']['detail_url'])){
      $image_data = file_get_contents($cover_image['meta']['detail_url']);
      $image_json = json_decode($image_data,'true');
      $row->setSourceProperty('image', $image_json['file']);
    }
    //Tags
      $terms = '';
      $tags = $row->getSourceProperty('tags');
      if(!empty($tags)){
         $_terms = $this->get_tids();
         $tags_data = array_intersect($_terms['tags'],$tags);
         $terms = array_keys($tags_data);
         $row->setSourceProperty('tags', $terms);
      }

    //Age group
    $age_group = $row->getSourceProperty('nav_tags');
    $age_term_id = '';
    if(!empty($age_group[0]['tag'])){
      $age_group_name = $age_group[0]['tag']['title'];
      if($age_group_name == '20'){
        $age_term_name = '20+';
      }
      else{
        $age_term_name = '15+';
      }
      $age_term_id = $this->get_term_id('age_groups',$age_term_name);
      $row->setSourceProperty('nav_tags',$age_term_id);
    }
    //objective
    $objective = $row->getSourceProperty('featured_in_latest');
    $objective_id = '17'; //term id for knowledge objective
    $row->setSourceProperty('featured_in_latest',$objective_id);

    return parent::prepareRow($row); 
  } 
 
private function get_term_id($vocabulary,$term_name) {
    $term_id = '';
    $language =  \Drupal::languageManager()->getCurrentLanguage()->getId();
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vocabulary);
    $tids = $query->execute();
    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);
    $termList = array();
    foreach($terms as $term) {
        if($term->hasTranslation($language)){
            $tid = $term->id();
            $translated_term = \Drupal::service('entity.repository')->getTranslationFromContext($term, $language);
            $termList[$tid] = strtolower($translated_term->getName());
        }
    }
    $term_name_search = strtolower($term_name);
    $term_id = array_search($term_name_search,$termList);
    if ($term_name == 'A GIRL’S BODY'){
      $term_id = '314';
     }
    if ($term_name == 'A BOY’S BODY'){
      $term_id = '315';
    }
    return $term_id;
  }


private function get_tids() {
    static $_terms;
    if (isset($_terms)) {
      return $_terms;
    }
    $_terms = [];
    $_terms['tags'] = $this->get_all_terms('tags');
    return $_terms;
  }

  private function get_all_terms($vocabulary) {
    $language =  \Drupal::languageManager()->getCurrentLanguage()->getId();
    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', $vocabulary);
    $tids = $query->execute();
    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);
    $termList = array();
    foreach($terms as $term) {
        if($term->hasTranslation($language)){
            $tid = $term->id();
            $term_name = $term->getName();
            $translated_term = \Drupal::service('entity.repository')->getTranslationFromContext($term, $language);
            $termList[$tid] = $translated_term->getName();
        }
    }
    return $termList;
  }
  
  public function getIds() { 
    $ids = [ 
      'id' => [ 
        'type' => 'string' 
      ] 
    ]; 
    return $ids; 
  } 
 
  public function fields() { 
    return array( 
      'title' => $this->t('Title'), 
      'body' => $this->t('Body'),
    ); 
  } 
 
  public function __toString() { 
    return "json data"; 
  } 
} 