(function ($, _, Drupal, drupalSettings) {
  'use strict';
  $(document).ready(function(){
  	$('.feeds-feed-form .js-form-item.form-item.js-form-type-textfield.form-type-textfield.js-form-item-title-0-value.form-item-title-0-value div#edit-title-0-value--description').text("The title for this source, always treated as non-markup plain text.");
	$('.path-feed div#block-mhealth-page-title h1.page-title').text("Add source");
	//alert($('div#block-mhealth-local-actions .action-links a.button.button-action.button--primary.button--small').text());
	if($('div#block-mhealth-local-actions .action-links a.button.button-action.button--primary.button--small').text() === 'Add feed'){
		$('div#block-mhealth-local-actions .action-links a.button.button-action.button--primary.button--small').text("Add Source");
	}
  });
  })(window.jQuery, window._, window.Drupal, window.drupalSettings);
